﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {

    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public int winScore = 10;
    public Text[] scoreText;
    public Text winText;
    static private Scorekeeper instance;

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

    // Use this for initialization
    void Start()
    {

        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError("More than one Scorekeeper exists in the scene.");
        }

        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";

        }
    }

    public void OnScoreGoal (int player)
    {
        score[player] += pointsPerGoal;
            scoreText[player].text = score[player].ToString();
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < score.Length; i++)
        {
            if (score[i] == winScore)
            {
                winText.text = "Player " + (i + 1) + " wins!";
                Time.timeScale = 0;
            }
        }
		
	}
}
