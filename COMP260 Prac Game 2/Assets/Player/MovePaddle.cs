﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

    public float speed = 20f;
    private Rigidbody rigidbody;

	// Use this for initialization
	void Start ()
    {

        rigidbody = GetComponent<Rigidbody>();

		
	}

    private void FixedUpdate()
    {
        /*Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        // check is the speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;*/

        Vector3 pos = Vector3.zero;
        float velZ = Input.GetAxis("Vertical");
        float velX = Input.GetAxis("Horizontal");

        if(velZ > 0)
            pos.z = 1;
        if(velZ < 0)
            pos.z = -1;   
        if (velX < 0)
            pos.x = -1;
        if (velX > 0)
            pos.x = 1;

        

        rigidbody.velocity = pos * speed;
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    private void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the xz plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0.0f;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
