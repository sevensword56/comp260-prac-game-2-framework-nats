﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class AIControl : MonoBehaviour {

    public Transform puck;
    public Transform goal;
    public float speed = 5.0f;
    private Rigidbody rigidbody;

    // Use this for initialization
    void Start () {

        rigidbody = GetComponent<Rigidbody>();

    }

    private void FixedUpdate()
    {
        Vector3 pos = Vector3.zero;

        Ray direction = new Ray(puck.position, goal.position);

        pos = direction.GetPoint(Vector3.Distance(puck.position, goal.position)/2);
        
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        // check is the speed going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;


    }

    // Update is called once per frame
    void Update () {
		
	}
}
